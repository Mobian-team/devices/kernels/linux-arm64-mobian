From: Sebastian Krzyszkowiak <sebastian.krzyszkowiak@puri.sm>
Date: Mon, 3 Aug 2020 23:59:37 +0200
Subject: hack: imx8m-ddrc: Set AHB and MAIN_AXI clocks together with DRAM
 freq

This is what NXP kernels do to achieve low power modes. See
https://source.codeaurora.org/external/imx/linux-imx/tree/drivers/soc/imx/busfreq-imx8mq.c?h=imx_5.4.70_2.3.0
and
https://source.codeaurora.org/external/imx/linux-imx/tree/arch/arm64/boot/dts/freescale/imx8mq.dtsi?h=imx_5.4.70_2.3.0#n267
---
 arch/arm64/boot/dts/freescale/imx8mq.dtsi |  8 +++--
 drivers/devfreq/imx8m-ddrc.c              | 49 +++++++++++++++++++++++++++++++
 2 files changed, 55 insertions(+), 2 deletions(-)

diff --git a/arch/arm64/boot/dts/freescale/imx8mq.dtsi b/arch/arm64/boot/dts/freescale/imx8mq.dtsi
index 64e0fb8..ca6d464 100644
--- a/arch/arm64/boot/dts/freescale/imx8mq.dtsi
+++ b/arch/arm64/boot/dts/freescale/imx8mq.dtsi
@@ -1678,11 +1678,15 @@ gic: interrupt-controller@38800000 {
 		ddrc: memory-controller@3d400000 {
 			compatible = "fsl,imx8mq-ddrc", "fsl,imx8m-ddrc";
 			reg = <0x3d400000 0x400000>;
-			clock-names = "core", "pll", "alt", "apb";
+			clock-names = "core", "pll", "alt", "apb", "main_axi", "ahb", "osc_25m", "sys2_pll_333m";
 			clocks = <&clk IMX8MQ_CLK_DRAM_CORE>,
 				 <&clk IMX8MQ_DRAM_PLL_OUT>,
 				 <&clk IMX8MQ_CLK_DRAM_ALT>,
-				 <&clk IMX8MQ_CLK_DRAM_APB>;
+				 <&clk IMX8MQ_CLK_DRAM_APB>,
+				 <&clk IMX8MQ_CLK_MAIN_AXI>,
+				 <&clk IMX8MQ_CLK_AHB>,
+				 <&clk IMX8MQ_CLK_25M>,
+				 <&clk IMX8MQ_SYS2_PLL_333M>;
 		};
 
 		ddr-pmu@3d800000 {
diff --git a/drivers/devfreq/imx8m-ddrc.c b/drivers/devfreq/imx8m-ddrc.c
index 1663697..a2b8981 100644
--- a/drivers/devfreq/imx8m-ddrc.c
+++ b/drivers/devfreq/imx8m-ddrc.c
@@ -71,6 +71,12 @@ struct imx8m_ddrc {
 	struct clk *dram_pll;
 	struct clk *dram_alt;
 	struct clk *dram_apb;
+	struct clk *main_axi;
+	struct clk *ahb;
+	struct clk *sys2_pll_333m;
+	struct clk *osc_25m;
+
+	unsigned long ahb_initial_freq;
 
 	int freq_count;
 	struct imx8m_ddrc_freq freq_table[IMX8M_DDRC_MAX_FREQ_COUNT];
@@ -268,6 +274,19 @@ static int imx8m_ddrc_target(struct device *dev, unsigned long *freq, u32 flags)
 		dev_dbg(dev, "ddrc freq set to %lu (was %lu)\n",
 			*freq, old_freq);
 
+	/* XXX hack inspired by drivers/soc/imx/busfreq-imx8mq.c in linux-imx */
+	if (DIV_ROUND_CLOSEST(new_freq, 250000) == 100) {
+		if (priv->ahb)
+			clk_set_rate(priv->ahb, priv->ahb_initial_freq / 6);
+		if (priv->main_axi && priv->osc_25m && priv->sys2_pll_333m)
+			clk_set_parent(priv->main_axi, priv->osc_25m);
+	} else {
+		if (priv->ahb)
+			clk_set_rate(priv->ahb, priv->ahb_initial_freq);
+		if (priv->main_axi && priv->osc_25m && priv->sys2_pll_333m)
+			clk_set_parent(priv->main_axi, priv->sys2_pll_333m);
+	}
+
 	return ret;
 }
 
@@ -406,6 +425,30 @@ static int imx8m_ddrc_probe(struct platform_device *pdev)
 		dev_err(dev, "failed to fetch apb clock: %d\n", ret);
 		return ret;
 	}
+	priv->main_axi = devm_clk_get(dev, "main_axi");
+	if (IS_ERR(priv->main_axi)) {
+		ret = PTR_ERR(priv->main_axi);
+		dev_err(dev, "failed to fetch main_axi clock: %d\n", ret);
+		priv->main_axi = NULL;
+	}
+	priv->ahb = devm_clk_get(dev, "ahb");
+	if (IS_ERR(priv->ahb)) {
+		ret = PTR_ERR(priv->ahb);
+		dev_err(dev, "failed to fetch ahb clock: %d\n", ret);
+		priv->ahb = NULL;
+	}
+	priv->osc_25m = devm_clk_get(dev, "osc_25m");
+	if (IS_ERR(priv->osc_25m)) {
+		ret = PTR_ERR(priv->osc_25m);
+		dev_err(dev, "failed to fetch osc_25m clock: %d\n", ret);
+		priv->osc_25m = NULL;
+	}
+	priv->sys2_pll_333m = devm_clk_get(dev, "sys2_pll_333m");
+	if (IS_ERR(priv->sys2_pll_333m)) {
+		ret = PTR_ERR(priv->sys2_pll_333m);
+		dev_err(dev, "failed to fetch sys2_pll_333m clock: %d\n", ret);
+		priv->sys2_pll_333m = NULL;
+	}
 
 	ret = dev_pm_opp_of_add_table(dev);
 	if (ret < 0) {
@@ -422,6 +465,12 @@ static int imx8m_ddrc_probe(struct platform_device *pdev)
 	priv->profile.get_cur_freq = imx8m_ddrc_get_cur_freq;
 	priv->profile.initial_freq = clk_get_rate(priv->dram_core);
 
+	priv->ahb_initial_freq = clk_get_rate(priv->ahb);
+	if (priv->ahb_initial_freq == 0) {
+		dev_err(dev, "could not read ahb clock rate\n");
+		priv->ahb_initial_freq = 133333333;
+	}
+
 	priv->devfreq = devm_devfreq_add_device(dev, &priv->profile,
 						gov, NULL);
 	if (IS_ERR(priv->devfreq)) {
