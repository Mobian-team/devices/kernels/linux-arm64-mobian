From: "Angus Ainslie (Purism)" <angus@akkea.ca>
Date: Tue, 28 Jan 2020 15:04:51 -0800
Subject: net: wireless: rsi: add the purism firmware loading

The purism redpine module loads the firmware from the onboard flash

Signed-off-by: Angus Ainslie (Purism) <angus@akkea.ca>
---
 drivers/net/wireless/rsi/Kconfig        |   8 +++
 drivers/net/wireless/rsi/rsi_91x_hal.c  | 101 +++++++++++++++++++++++++++++++-
 drivers/net/wireless/rsi/rsi_91x_main.c |  25 +++++++-
 drivers/net/wireless/rsi/rsi_91x_mgmt.c |   6 ++
 drivers/net/wireless/rsi/rsi_hal.h      |   6 ++
 drivers/net/wireless/rsi/rsi_main.h     |   6 ++
 drivers/net/wireless/rsi/rsi_mgmt.h     |   9 ++-
 7 files changed, 156 insertions(+), 5 deletions(-)

diff --git a/drivers/net/wireless/rsi/Kconfig b/drivers/net/wireless/rsi/Kconfig
index 09a5924..731056a 100644
--- a/drivers/net/wireless/rsi/Kconfig
+++ b/drivers/net/wireless/rsi/Kconfig
@@ -54,4 +54,12 @@ config RSI_COEX
 	  Select M (recommended), if you have want to use this feature
 	  and you have RS9113 module.
 
+config RSI_PURISM
+	bool "Redpine Signals PURISM FW support"
+	depends on RSI_91X && RSI_COEX
+	default n
+	help
+	  This option enables the PURISM FW support.
+	  Say Y if you want to use this feature.
+
 endif # WLAN_VENDOR_RSI
diff --git a/drivers/net/wireless/rsi/rsi_91x_hal.c b/drivers/net/wireless/rsi/rsi_91x_hal.c
index dca81a4..784f71f 100644
--- a/drivers/net/wireless/rsi/rsi_91x_hal.c
+++ b/drivers/net/wireless/rsi/rsi_91x_hal.c
@@ -845,7 +845,6 @@ static int rsi_hal_prepare_fwload(struct rsi_hw *adapter)
 	int status;
 
 	bl_start_cmd_timer(adapter, BL_CMD_TIMEOUT);
-
 	while (!adapter->blcmd_timer_expired) {
 		status = hif_ops->master_reg_read(adapter, SWBL_REGOUT,
 						  &regout_val,
@@ -999,6 +998,98 @@ static int rsi_load_9113_firmware(struct rsi_hw *adapter)
 	return status;
 }
 
+static int rsi_load_9116_flash_fw(struct rsi_hw *adapter)
+{
+	struct rsi_host_intf_ops *hif_ops = adapter->host_intf_ops;
+	struct rsi_common *common = adapter->priv;
+	u8 *fw_p;
+	u8 flash_read[RSI_MAX_FLASH_OFFSET_SIZE];
+	int status;
+	int image_type;
+
+	status = hif_ops->master_reg_write(adapter, MEM_ACCESS_CTRL_FROM_HOST,
+					   RAM_384K_ACCESS_FROM_TA,
+					   RSI_9116_REG_SIZE);
+	if (status < 0) {
+		rsi_dbg(ERR_ZONE, "%s: Unable to access full RAM memory\n",
+			__func__);
+		return status;
+	}
+
+	rsi_dbg(INIT_ZONE, "%s: loading firmware from flash\n", __func__);
+
+	if (adapter->priv->coex_mode == 2 || adapter->priv->coex_mode == 4)
+		image_type = RSI_FLASH_READ_COEX_IMAGE;
+	else
+		image_type = RSI_FLASH_READ_WLAN_IMAGE;
+
+	status = hif_ops->master_reg_read(adapter,
+					image_type,
+					(int *)flash_read,
+					2);
+	if (status < 0) {
+		rsi_dbg(ERR_ZONE,
+			"%s: RSI_FLASH_READ failed\n", __func__);
+		return status;
+	}
+	fw_p = flash_read;
+
+	if (*(u16 *)fw_p == RSI_9116_FW_MAGIC_WORD) {
+		rsi_dbg(ERR_ZONE, "***** Loading Firmware from Flash *****\n");
+		if ((hif_ops->master_reg_write(adapter, MEM_ACCESS_CTRL_FROM_HOST,
+					       RAM_384K_ACCESS_FROM_TA, 4)) < 0) {
+			rsi_dbg(ERR_ZONE, "%s: Unable to access full RAM memory\n",
+				    __func__);
+			return -EIO;
+		}
+		if ((bl_cmd(adapter, LOAD_HOSTED_FW, LOADING_INITIATED,
+			    "LOAD_HOSTED_FW")) < 0){
+			rsi_dbg(ERR_ZONE, "%s: FW_LOAD_BL_CMD failed\n", __func__);
+			return -EIO;
+		}
+		//status = rsi_load_9116_flash_fw(adapter);
+		mdelay(3000);
+		status = hif_ops->master_reg_read(adapter,
+				RSI_FLASH_READ_FW_VER,
+				(int *)fw_p, 4);
+		if (status < 0) {
+			rsi_dbg(ERR_ZONE,
+				    "%s: RSI_FLASH_READ_FW_VER failed\n", __func__);
+			return status;
+		}
+		common->lmac_ver.major = fw_p[2];
+		common->lmac_ver.minor = fw_p[3];
+		status = hif_ops->master_reg_read(adapter,
+				RSI_FLASH_READ_FW_VER1,
+				(int *)fw_p, 4);
+		if (status < 0) {
+			rsi_dbg(ERR_ZONE,
+				    "%s: RSI_FLASH_READ_FW_VER1 failed\n", __func__);
+			return status;
+		}
+		common->lmac_ver.release_num = fw_p[0];
+		common->lmac_ver.patch_num = fw_p[2];
+		rsi_print_version(common);
+		if (adapter->rsi_host_intf == RSI_HOST_INTF_USB) {
+			if (bl_cmd(adapter, POLLING_MODE,
+				   CMD_PASS, "POLLING_MODE") < 0) {
+				rsi_dbg(ERR_ZONE,
+					    "%s: USB POLLING_MODE failed\n",
+					    __func__);
+			}
+		}
+	}
+	else
+	{
+		rsi_dbg(ERR_ZONE, "Flash firmware load failed - incorrect magic\n");
+		return -EIO;
+	}
+
+	rsi_dbg(ERR_ZONE, "***** Loaded Firmware to RAM - Waiting for Card Ready *****\n");
+
+	return status;
+}
+
 static int rsi_load_9116_firmware(struct rsi_hw *adapter)
 {
 	struct rsi_common *common = adapter->priv;
@@ -1149,7 +1240,13 @@ int rsi_hal_device_init(struct rsi_hw *adapter)
 		status = rsi_hal_prepare_fwload(adapter);
 		if (status < 0)
 			return status;
-		if (rsi_load_9116_firmware(adapter)) {
+
+		if (common->load_flash_fw)
+			status = rsi_load_9116_flash_fw(adapter);
+		else
+			status = rsi_load_9116_firmware(adapter);
+
+		if (status) {
 			rsi_dbg(ERR_ZONE,
 				"%s: Failed to load firmware to 9116 device\n",
 				__func__);
diff --git a/drivers/net/wireless/rsi/rsi_91x_main.c b/drivers/net/wireless/rsi/rsi_91x_main.c
index f9f0044..574b85a 100644
--- a/drivers/net/wireless/rsi/rsi_91x_main.c
+++ b/drivers/net/wireless/rsi/rsi_91x_main.c
@@ -45,6 +45,21 @@ static struct rsi_proto_ops g_proto_ops = {
 };
 #endif
 
+static u8 antenna_sel = 2;
+module_param(antenna_sel, byte, 0);
+MODULE_PARM_DESC(antenna_sel, "\n Antenna selection. '2' for  intenal antenna \
+and '3' for External antenna\n");
+
+static u16 feature_bitmap_9116 = 0;
+module_param(feature_bitmap_9116, ushort, 0);
+MODULE_PARM_DESC(feature_bitmap_9116, "\n9116 Feature Bitmap BIT(0) 0: AGC_PD \
+Enable, 1: AGC_PD Disable BIT(7:1) Reserved\n");
+
+static bool load_flash_fw = 0;
+module_param(load_flash_fw, bool, 0);
+MODULE_PARM_DESC(load_flash_fw, "\n Load firmware from on board flash\n\
+'0' for disable and '1' for enable\n");
+
 /**
  * rsi_dbg() - This function outputs informational messages.
  * @zone: Zone of interest for output message.
@@ -104,7 +119,8 @@ void rsi_print_version(struct rsi_common *common)
 		common->lmac_ver.release_num);
 	rsi_dbg(ERR_ZONE, "Operating mode\t: %d [%s]",
 		common->oper_mode, opmode_str(common->oper_mode));
-	rsi_dbg(ERR_ZONE, "Firmware file\t: %s", common->priv->fw_file_name);
+	if (!common->load_flash_fw)
+		rsi_dbg(ERR_ZONE, "Firmware file\t: %s", common->priv->fw_file_name);
 	rsi_dbg(ERR_ZONE, "================================================\n");
 }
 
@@ -343,6 +359,8 @@ struct rsi_hw *rsi_91x_init(u16 oper_mode)
 	init_completion(&common->wlan_init_completion);
 	adapter->device_model = RSI_DEV_9113;
 	common->oper_mode = oper_mode;
+	common->load_flash_fw = load_flash_fw;
+	common->obm_ant_sel_val = antenna_sel;
 
 	/* Determine coex mode */
 	switch (common->oper_mode) {
@@ -356,7 +374,10 @@ struct rsi_hw *rsi_91x_init(u16 oper_mode)
 		break;
 	case DEV_OPMODE_AP_BT_DUAL:
 	case DEV_OPMODE_AP_BT:
-		common->coex_mode = 4;
+		if (load_flash_fw)
+			common->coex_mode = 2;
+		else
+			common->coex_mode = 4;
 		break;
 	case DEV_OPMODE_WIFI_ALONE:
 		common->coex_mode = 1;
diff --git a/drivers/net/wireless/rsi/rsi_91x_mgmt.c b/drivers/net/wireless/rsi/rsi_91x_mgmt.c
index 0848f7a..d48e168 100644
--- a/drivers/net/wireless/rsi/rsi_91x_mgmt.c
+++ b/drivers/net/wireless/rsi/rsi_91x_mgmt.c
@@ -900,6 +900,12 @@ static int rsi_send_common_dev_params(struct rsi_common *common)
 	dev_cfgs->driver_mode = common->driver_mode;
 	dev_cfgs->region_code = NL80211_DFS_FCC;
 	dev_cfgs->antenna_sel_val = common->obm_ant_sel_val;
+	if (common->load_flash_fw) {
+		dev_cfgs->features_9116 = (common->ext_opt & 0xF) |
+					  (common->host_intf_on_demand << 4) |
+					  (common->crystal_as_sleep_clk << 5) |
+					  (common->feature_bitmap_9116 << 11);
+	}
 
 	skb_put(skb, frame_len);
 
diff --git a/drivers/net/wireless/rsi/rsi_hal.h b/drivers/net/wireless/rsi/rsi_hal.h
index 5b07262..04f940a 100644
--- a/drivers/net/wireless/rsi/rsi_hal.h
+++ b/drivers/net/wireless/rsi/rsi_hal.h
@@ -147,6 +147,12 @@
 #define FW_ALIGN_SIZE			4
 #define RSI_9116_FW_MAGIC_WORD		0x5aa5
 
+#define RSI_FLASH_READ_COEX_IMAGE       (0x04000000 + 0x80000 + 0x40)
+#define RSI_FLASH_READ_WLAN_IMAGE       (0x04000000 + 0x20000 + 0x40)
+#define RSI_FLASH_READ_FW_VER		0x00002c04
+#define RSI_FLASH_READ_FW_VER1		0x00002c08
+#define RSI_MAX_FLASH_OFFSET_SIZE	0x4
+
 #define MEM_ACCESS_CTRL_FROM_HOST	0x41300000
 #define RAM_384K_ACCESS_FROM_TA		(BIT(2) | BIT(3) | BIT(4) | BIT(5) | \
 					 BIT(20) | BIT(21) | BIT(22) | \
diff --git a/drivers/net/wireless/rsi/rsi_main.h b/drivers/net/wireless/rsi/rsi_main.h
index dcf8fb4..a5b7807 100644
--- a/drivers/net/wireless/rsi/rsi_main.h
+++ b/drivers/net/wireless/rsi/rsi_main.h
@@ -305,6 +305,12 @@ struct rsi_common {
 	u8 obm_ant_sel_val;
 	int tx_power;
 	u8 ant_in_use;
+	u8 load_flash_fw;
+	u16 ext_opt;
+	u8 host_intf_on_demand;
+	u8 crystal_as_sleep_clk;
+	u16 feature_bitmap_9116;
+
 	/* Mutex used for writing packet to bus */
 	struct mutex tx_bus_mutex;
 	bool hibernate_resume;
diff --git a/drivers/net/wireless/rsi/rsi_mgmt.h b/drivers/net/wireless/rsi/rsi_mgmt.h
index 236b214..7b222ab 100644
--- a/drivers/net/wireless/rsi/rsi_mgmt.h
+++ b/drivers/net/wireless/rsi/rsi_mgmt.h
@@ -598,7 +598,14 @@ struct rsi_config_vals {
 	u8 driver_mode;
 	u8 region_code;
 	u8 antenna_sel_val;
-	u8 reserved2[16];
+	u16 dev_peer_dist;
+	u16 dev_bt_feature_bitmap;
+	u16 uart_dbg;
+	u16 features_9116;
+	u16 dev_ble_roles;
+	u16 bt_bdr;
+	u16 dev_anchor_point_gap;
+	u8 reserved2[2];
 } __packed;
 
 /* Packet info flags */
