From: Sebastian Krzyszkowiak <sebastian.krzyszkowiak@puri.sm>
Date: Sat, 5 Mar 2022 14:57:39 +0100
Subject: power: supply: bq25890_charger: Throttle ADC to 1 sec

When using continuous conversions, the controller provides new data
every one second. The driver is using continuous conversions when
there's external power available, but switches to one-shot conversions
when it's not. One-shot conversion is triggered every time a property
that requires ADC is being read, which can trigger several conversions
one after another. This turns out to be especially problematic when the
battery is too hot, as the controller sends an interrupt after ADC,
which triggers the driver to read all properties, which in turn causes
several more ADC conversions, taxing the system with unnecessary load.

To solve these issues, throttle triggering ADC to one second, making
it behave similarly to continuous conversions done by the controller.

Signed-off-by: Sebastian Krzyszkowiak <sebastian.krzyszkowiak@puri.sm>
---
 drivers/power/supply/bq25890_charger.c | 11 ++++++++++-
 1 file changed, 10 insertions(+), 1 deletion(-)

diff --git a/drivers/power/supply/bq25890_charger.c b/drivers/power/supply/bq25890_charger.c
index 5f8a8a6..cd4bb10 100644
--- a/drivers/power/supply/bq25890_charger.c
+++ b/drivers/power/supply/bq25890_charger.c
@@ -13,6 +13,7 @@
 #include <linux/gpio/consumer.h>
 #include <linux/interrupt.h>
 #include <linux/delay.h>
+#include <linux/sched/clock.h>
 #include <linux/usb/phy.h>
 #include <linux/regulator/consumer.h>
 
@@ -124,6 +125,7 @@ struct bq25890_device {
 
 	struct mutex lock; /* protect state data */
 	struct gpio_desc *otg_en;
+	unsigned long long adc_timestamp;
 };
 
 static const struct regmap_range bq25890_readonly_reg_ranges[] = {
@@ -458,8 +460,13 @@ static int bq25890_power_supply_get_property(struct power_supply *psy,
 	__bq25890_handle_irq(bq);
 	state = bq->state;
 	do_adc_conv = !state.online && bq25890_is_adc_property(psp);
-	if (do_adc_conv)
+	/* throttle ADC conversions to 1s */
+	if (sched_clock() - bq->adc_timestamp < NSEC_PER_SEC)
+		do_adc_conv = false;
+	if (do_adc_conv) {
+		bq->adc_timestamp = sched_clock();
 		bq25890_field_write(bq, F_CONV_START, 1);
+	}
 	mutex_unlock(&bq->lock);
 
 	if (do_adc_conv)
@@ -709,6 +716,7 @@ static irqreturn_t __bq25890_handle_irq(struct bq25890_device *bq)
 		ret = bq25890_field_write(bq, F_CONV_START, 1);
 		if (ret < 0)
 			goto error;
+		bq->adc_timestamp = sched_clock();
 	}
 
 	bq->state = new_state;
@@ -815,6 +823,7 @@ static int bq25890_hw_init(struct bq25890_device *bq)
 		dev_dbg(bq->dev, "Config ADC failed %d\n", ret);
 		return ret;
 	}
+	bq->adc_timestamp = sched_clock();
 
 	/* Configure ADC for continuous conversions when charging */
 	ret = bq25890_field_write(bq, F_CONV_RATE, !!bq->state.online);
