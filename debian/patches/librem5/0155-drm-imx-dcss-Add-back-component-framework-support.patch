From: Laurentiu Palcu <laurentiu.palcu@nxp.com>
Date: Fri, 31 Jul 2020 13:31:59 +0300
Subject: drm/imx/dcss: Add back component framework support

Since HDP driver still uses it, add support back to DCSS driver until HDP gets
rid of it.

Signed-off-by: Laurentiu Palcu <laurentiu.palcu@nxp.com>
---
 drivers/gpu/drm/imx/dcss/dcss-dev.c |  6 ++-
 drivers/gpu/drm/imx/dcss/dcss-dev.h |  1 +
 drivers/gpu/drm/imx/dcss/dcss-drv.c | 96 ++++++++++++++++++++++++++++---------
 drivers/gpu/drm/imx/dcss/dcss-kms.c | 31 ++++++++----
 drivers/gpu/drm/imx/dcss/dcss-kms.h |  4 +-
 5 files changed, 104 insertions(+), 34 deletions(-)

diff --git a/drivers/gpu/drm/imx/dcss/dcss-dev.c b/drivers/gpu/drm/imx/dcss/dcss-dev.c
index 38bc28a..1ce8f58 100644
--- a/drivers/gpu/drm/imx/dcss/dcss-dev.c
+++ b/drivers/gpu/drm/imx/dcss/dcss-dev.c
@@ -289,7 +289,8 @@ int dcss_dev_suspend(struct device *dev)
 	struct dcss_kms_dev *kms = container_of(ddev, struct dcss_kms_dev, base);
 	int ret;
 
-	drm_bridge_connector_disable_hpd(kms->connector);
+	if (!dcss_drv_is_componentized(dev))
+		drm_bridge_connector_disable_hpd(kms->connector);
 
 	drm_mode_config_helper_suspend(ddev);
 
@@ -328,7 +329,8 @@ int dcss_dev_resume(struct device *dev)
 
 	drm_mode_config_helper_resume(ddev);
 
-	drm_bridge_connector_enable_hpd(kms->connector);
+	if (!dcss_drv_is_componentized(dev))
+		drm_bridge_connector_enable_hpd(kms->connector);
 
 	return 0;
 }
diff --git a/drivers/gpu/drm/imx/dcss/dcss-dev.h b/drivers/gpu/drm/imx/dcss/dcss-dev.h
index 6d9aadf..1f1348b 100644
--- a/drivers/gpu/drm/imx/dcss/dcss-dev.h
+++ b/drivers/gpu/drm/imx/dcss/dcss-dev.h
@@ -96,6 +96,7 @@ struct dcss_dev {
 
 struct dcss_dev *dcss_drv_dev_to_dcss(struct device *dev);
 struct drm_device *dcss_drv_dev_to_drm(struct device *dev);
+bool dcss_drv_is_componentized(struct device *dev);
 struct dcss_dev *dcss_dev_create(struct device *dev, bool hdmi_output);
 void dcss_dev_destroy(struct dcss_dev *dcss);
 int dcss_dev_runtime_suspend(struct device *dev);
diff --git a/drivers/gpu/drm/imx/dcss/dcss-drv.c b/drivers/gpu/drm/imx/dcss/dcss-drv.c
index 8dc2f85..f8ca943 100644
--- a/drivers/gpu/drm/imx/dcss/dcss-drv.c
+++ b/drivers/gpu/drm/imx/dcss/dcss-drv.c
@@ -6,6 +6,7 @@
 #include <linux/module.h>
 #include <linux/kernel.h>
 #include <linux/platform_device.h>
+#include <linux/component.h>
 #include <drm/drm_of.h>
 
 #include "dcss-dev.h"
@@ -14,6 +15,8 @@
 struct dcss_drv {
 	struct dcss_dev *dcss;
 	struct dcss_kms_dev *kms;
+
+	bool is_componentized;
 };
 
 struct dcss_dev *dcss_drv_dev_to_dcss(struct device *dev)
@@ -30,30 +33,25 @@ struct drm_device *dcss_drv_dev_to_drm(struct device *dev)
 	return mdrv ? &mdrv->kms->base : NULL;
 }
 
-static int dcss_drv_platform_probe(struct platform_device *pdev)
+bool dcss_drv_is_componentized(struct device *dev)
 {
-	struct device *dev = &pdev->dev;
-	struct device_node *remote;
-	struct dcss_drv *mdrv;
-	int err = 0;
-	bool hdmi_output = true;
-
-	if (!dev->of_node)
-		return -ENODEV;
-
-	remote = of_graph_get_remote_node(dev->of_node, 0, 0);
-	if (!remote)
-		return -ENODEV;
+	struct dcss_drv *mdrv = dev_get_drvdata(dev);
 
-	hdmi_output = !of_device_is_compatible(remote, "fsl,imx8mq-nwl-dsi");
+	return mdrv->is_componentized;
+}
 
-	of_node_put(remote);
+static int dcss_drv_init(struct device *dev, bool componentized)
+{
+	struct dcss_drv *mdrv;
+	int err = 0;
 
 	mdrv = kzalloc(sizeof(*mdrv), GFP_KERNEL);
 	if (!mdrv)
 		return -ENOMEM;
 
-	mdrv->dcss = dcss_dev_create(dev, hdmi_output);
+	mdrv->is_componentized = componentized;
+
+	mdrv->dcss = dcss_dev_create(dev, componentized);
 	if (IS_ERR(mdrv->dcss)) {
 		err = PTR_ERR(mdrv->dcss);
 		goto err;
@@ -61,7 +59,7 @@ static int dcss_drv_platform_probe(struct platform_device *pdev)
 
 	dev_set_drvdata(dev, mdrv);
 
-	mdrv->kms = dcss_kms_attach(mdrv->dcss);
+	mdrv->kms = dcss_kms_attach(mdrv->dcss, componentized);
 	if (IS_ERR(mdrv->kms)) {
 		err = PTR_ERR(mdrv->kms);
 		goto dcss_shutoff;
@@ -79,19 +77,73 @@ static int dcss_drv_platform_probe(struct platform_device *pdev)
 	return err;
 }
 
-static int dcss_drv_platform_remove(struct platform_device *pdev)
+static void dcss_drv_deinit(struct device *dev, bool componentized)
 {
-	struct dcss_drv *mdrv = dev_get_drvdata(&pdev->dev);
+	struct dcss_drv *mdrv = dev_get_drvdata(dev);
 
 	if (!mdrv)
-		return 0;
+		return;
 
-	dcss_kms_detach(mdrv->kms);
+	dcss_kms_detach(mdrv->kms, componentized);
 	dcss_dev_destroy(mdrv->dcss);
 
-	dev_set_drvdata(&pdev->dev, NULL);
+	dev_set_drvdata(dev, NULL);
 
 	kfree(mdrv);
+}
+
+static int dcss_drv_bind(struct device *dev)
+{
+	return dcss_drv_init(dev, true);
+}
+
+static void dcss_drv_unbind(struct device *dev)
+{
+	return dcss_drv_deinit(dev, true);
+}
+
+static const struct component_master_ops dcss_master_ops = {
+	.bind	= dcss_drv_bind,
+	.unbind	= dcss_drv_unbind,
+};
+
+static int compare_of(struct device *dev, void *data)
+{
+	return dev->of_node == data;
+}
+
+static int dcss_drv_platform_probe(struct platform_device *pdev)
+{
+	struct device *dev = &pdev->dev;
+	struct component_match *match = NULL;
+	struct device_node *remote;
+
+	if (!dev->of_node)
+		return -ENODEV;
+
+	remote = of_graph_get_remote_node(dev->of_node, 0, 0);
+	if (!remote)
+		return -ENODEV;
+
+	if (of_device_is_compatible(remote, "fsl,imx8mq-nwl-dsi")) {
+		of_node_put(remote);
+		return dcss_drv_init(dev, false);
+	}
+
+	drm_of_component_match_add(dev, &match, compare_of, remote);
+	of_node_put(remote);
+
+	return component_master_add_with_match(dev, &dcss_master_ops, match);
+}
+
+static int dcss_drv_platform_remove(struct platform_device *pdev)
+{
+	struct dcss_drv *mdrv = dev_get_drvdata(&pdev->dev);
+
+	if (mdrv->is_componentized)
+		component_master_del(&pdev->dev, &dcss_master_ops);
+	else
+		dcss_drv_deinit(&pdev->dev, false);
 
 	return 0;
 }
diff --git a/drivers/gpu/drm/imx/dcss/dcss-kms.c b/drivers/gpu/drm/imx/dcss/dcss-kms.c
index 9b84df3..5a57014 100644
--- a/drivers/gpu/drm/imx/dcss/dcss-kms.c
+++ b/drivers/gpu/drm/imx/dcss/dcss-kms.c
@@ -13,6 +13,7 @@
 #include <drm/drm_of.h>
 #include <drm/drm_probe_helper.h>
 #include <drm/drm_vblank.h>
+#include <linux/component.h>
 
 #include "dcss-dev.h"
 #include "dcss-kms.h"
@@ -107,7 +108,7 @@ static int dcss_kms_bridge_connector_init(struct dcss_kms_dev *kms)
 	return 0;
 }
 
-struct dcss_kms_dev *dcss_kms_attach(struct dcss_dev *dcss)
+struct dcss_kms_dev *dcss_kms_attach(struct dcss_dev *dcss, bool componentized)
 {
 	struct dcss_kms_dev *kms;
 	struct drm_device *drm;
@@ -130,19 +131,28 @@ struct dcss_kms_dev *dcss_kms_attach(struct dcss_dev *dcss)
 	if (ret)
 		goto cleanup_mode_config;
 
-	ret = dcss_kms_bridge_connector_init(kms);
-	if (ret)
-		goto cleanup_mode_config;
+	if (!componentized) {
+		ret = dcss_kms_bridge_connector_init(kms);
+		if (ret)
+			goto cleanup_mode_config;
+	}
 
 	ret = dcss_crtc_init(crtc, drm);
 	if (ret)
 		goto cleanup_mode_config;
 
+	if (componentized) {
+		ret = component_bind_all(dcss->dev, kms);
+		if (ret)
+			goto cleanup_crtc;
+	}
+
 	drm_mode_config_reset(drm);
 
 	drm_kms_helper_poll_init(drm);
 
-	drm_bridge_connector_enable_hpd(kms->connector);
+	if (!componentized)
+		drm_bridge_connector_enable_hpd(kms->connector);
 
 	ret = drm_dev_register(drm, 0);
 	if (ret)
@@ -153,7 +163,8 @@ struct dcss_kms_dev *dcss_kms_attach(struct dcss_dev *dcss)
 	return kms;
 
 cleanup_crtc:
-	drm_bridge_connector_disable_hpd(kms->connector);
+	if (!componentized)
+		drm_bridge_connector_disable_hpd(kms->connector);
 	drm_kms_helper_poll_fini(drm);
 	dcss_crtc_deinit(crtc, drm);
 
@@ -164,16 +175,20 @@ struct dcss_kms_dev *dcss_kms_attach(struct dcss_dev *dcss)
 	return ERR_PTR(ret);
 }
 
-void dcss_kms_detach(struct dcss_kms_dev *kms)
+void dcss_kms_detach(struct dcss_kms_dev *kms, bool componentized)
 {
 	struct drm_device *drm = &kms->base;
+	struct dcss_dev *dcss = drm->dev_private;
 
 	drm_dev_unregister(drm);
-	drm_bridge_connector_disable_hpd(kms->connector);
+	if (!componentized)
+		drm_bridge_connector_disable_hpd(kms->connector);
 	drm_kms_helper_poll_fini(drm);
 	drm_atomic_helper_shutdown(drm);
 	drm_crtc_vblank_off(&kms->crtc.base);
 	drm_mode_config_cleanup(drm);
 	dcss_crtc_deinit(&kms->crtc, drm);
+	if (componentized)
+		component_unbind_all(dcss->dev, drm);
 	drm->dev_private = NULL;
 }
diff --git a/drivers/gpu/drm/imx/dcss/dcss-kms.h b/drivers/gpu/drm/imx/dcss/dcss-kms.h
index dfe5dd9..c3e0d06 100644
--- a/drivers/gpu/drm/imx/dcss/dcss-kms.h
+++ b/drivers/gpu/drm/imx/dcss/dcss-kms.h
@@ -32,8 +32,8 @@ struct dcss_kms_dev {
 	struct drm_connector *connector;
 };
 
-struct dcss_kms_dev *dcss_kms_attach(struct dcss_dev *dcss);
-void dcss_kms_detach(struct dcss_kms_dev *kms);
+struct dcss_kms_dev *dcss_kms_attach(struct dcss_dev *dcss, bool componetized);
+void dcss_kms_detach(struct dcss_kms_dev *kms, bool componetized);
 int dcss_crtc_init(struct dcss_crtc *crtc, struct drm_device *drm);
 void dcss_crtc_deinit(struct dcss_crtc *crtc, struct drm_device *drm);
 struct dcss_plane *dcss_plane_init(struct drm_device *drm,
